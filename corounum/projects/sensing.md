---
layout: default
title: Open Sensing
---

# Open Environmental Sensing : Design Constraints

Many environmental sensing projects are either based on closed hardware that is very expensive, open hardware that is bespoke (and therefore expensive in time and potentially error- or failure-prone in construction), or bespoke manufactured... potentially making it more robust, but still expensive.

The design constraints we set for ourselves are myriad and manifold:

1. **Develop open hardware and software solutions**
	There are many projects that are engaging in this space.
  
1. **Develop low-cost solutions**
	Many solutions in this space are *expensive*. We want solutions that are
	ultimately in the $5-$20 space.
  
1. **Design for manufacture**
	Many boards are designed as one-off solutions that must be hand fabricated,
	or require specialty fab that cannot be produced in volume. We will use [SparkFun's guide for selling your widget](https://learn.sparkfun.com/tutorials/how-to-sell-your-widget-on-sparkfun) as a reference.
  
1. **Design for battery**
	Many hobbyist solutions to sensing are cobbled together from COTS parts. They either require heavy modification, or cannot be modified, to operate for long life on battery. Our minimum target is 6 months, and we want our designs to generally last 1 year or more on 3xAA batteries.
  
1. **Design for modularity**
	Although Grove, Quiic, and other "modular" solutions exist for hobbyist electronics, they all fail one or more of our criteria. For example, both Grove and Quiic fail to provide any way to toggle power on the modules that are quickly assembled. Therefore, it is impossible to design for operation on battery and achieve our desired lifetimes. Our modular solution must allow for the easy (and integral) toggling of power, while also being appropriate for manufacture.
  
1. **Design for sensing**
  It seems obvious, but our modularity should be with a mind to environmental sensing. We are not going to try and solve every electronic problem, but we are going to try and make it possible to have an ecosystem of power modules, CPU modules, sensing modules, storage modules, and radio modules that can be interchanged. 
	
For example, there are modular solutions to assembling electronic components---Grove from Seeed and Quiic from Sparkfun come to mind---but neither accommodates *design for battery*. They are quick-build frameworks that assume that everything you connect will *always* be on *all* the time. If we're going to be modular, it needs to allow us to turn the modules on-and-off.

Further, by designing for a specific class of problems, we can implement solutions that are more appropriate to our task space. For example, a sensor that is indoors vs. outdoors might vary in storage and radio... the indoor version might have no storage, but use WiFi; the outdoor version might have storage and no radio, because the power costs are too high to broadcast data back to a base station. However, the two units might use the same base board, and the same power module, and the same CPU.

Ultimately, our design work supports scientific inquiry into the world around us. But, we'd like to be able to do more studies in more locations, and therefore need a way to get per-sensor costs *down*, which we believe we can do through an open and iterative design/test process.