---
title: "Projects"
layout: default
---

# Projects

Group projects are generally categorized as active (someone is working on them), dormant (the project is looking for someone to pick it up), or archived.

## Active Projects

<ul> 
{% for proj in site.projects %}
  {% if proj.status == "active" %}
    <li>
      <a href="{{proj.url}}.html">{{proj.title}}</a>
      <br>{{proj.blurb}}
    </li>
  {% endif %}
{% endfor %}
</ul>

## Dormant Projects

<ul> 
{% for proj in site.projects %}
  {% if proj.status == "dormant" %}
    <li>
      <a href="{{proj.url}}.html">{{proj.title}}</a>
      <br>{{proj.blurb}}
    </li>
  {% endif %}
{% endfor %}
</ul>


## Archived Projects

<ul> 
{% for proj in site.projects %}
  {% if proj.status == "archived" %}
    <li>
      <a href="{{proj.url}}.html">{{proj.title}}</a>
      <br>{{proj.blurb}}
    </li>
  {% endif %}
{% endfor %}
</ul>
