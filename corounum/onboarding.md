---
title: Tools and Resources for the Coro Unum Group
layout: default
---

<script src="https://trello.com/1/client.js"></script>

# Onboarding

We'll [use Trello to manage our group process](https://trello.com/b/He84NLQf/coro-unum-group) in a number of ways. It is public, which is good (we're an open research group, meaning we work publicly, and share our work publicly), and it is easy for everyone to contribute. As members come and go from the work, it is easy to see the history, and to continue contributing. 


## Community and Culture

<div class="row">
  <div class="col-sm">
    We aspire to be anti-racist and anti-sexist in our work—which, if those words are new to you, you could read them to mean "we try and create an equitable and enjoyable workplace for <i>everyone</i> in the group."
  </div>
  <div class="col-sm">
    <blockquote class="trello-card-compact">
      <a href="https://trello.com/c/PLpCN8d7/4-code-of-conduct">Code of Conduct</a>
    </blockquote>
    <script src="https://p.trellocdn.com/embed.min.js"></script>
  </div>
</div>

## Tools

<div class="row">
  <div class="col-sm">
    We'll use a wide variety of tools in our work. This Trello card links to the tools the group is currently using for storing, collaborating on, and managing documents and data of all sorts.
  </div>
  <div class="col-sm">
    <blockquote class="trello-card-compact">
      <a href="https://trello.com/c/8H76FV4I/2-tools">Tools</a>
    </blockquote>
    <script src="https://p.trellocdn.com/embed.min.js"></script>
  </div>
</div>

## Guides

<div class="row">
  <div class="col-sm">
    No one expects you to know everything on day one. Or, on day two. Really, no one expects you to know everything <i>ever</i>. Always be encouraged to ask Matt, or other members of the team, for guidance and help. Over time, we've collected resources that we think help bring people up to speed on the tools we use.
  </div>
  <div class="col-sm">
    <blockquote class="trello-card-compact">
      <a href="https://trello.com/c/jOTOZzoQ/10-guides-and-resources">Guides and Resources</a>
    </blockquote>
    <script src="https://p.trellocdn.com/embed.min.js"></script>
  </div>
</div>