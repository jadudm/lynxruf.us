---
title: Projects
layout: default
---


# Projects

{% for proj in site.projects %}
<div class="row">
	<h2>{{proj.title}}</h2>
	<p>Summary: {{proj.blurb}}</p>
	<p>Link: <a href="{{proj.url}}">{{proj.url}}</a></p>
	<p>Repository: <a href="{{proj.repository}}">{{proj.repos-host}}</a></p>
  <p>Contributors: {{proj.contributors}}</p>
</div>

{% endfor %}

<blockquote class="trello-card">
  <a href="https://trello.com/c/QSLEkNah/1-testing">Trello Card</a>
</blockquote>

<br>

<blockquote class="trello-board-compact">
  <a href="https://trello.com/b/He84NLQf/sensing">Trello Board</a>
</blockquote>

<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="https://trello.com/1/client.js"></script>

