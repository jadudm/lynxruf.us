---
layout: default
title: Research - The Coro Unum Group
---

# The Coro Unum Group

The Coro Unum Group is in search of open source/open culture solutions to challenging problems. We aspire to solutions that are affordable in implementation and replication, be those solutions social, technical, or otherwise. 

We adhere to the [contributor code of conduct]({{site.baseurl}}/corounum/codeofconduct.html) in this group, and in doing so, aspire to do *amazing* research in a collaborative and inclusive manner.


## Projects

The group has [multiple projects that are ongoing](projects/), and those projects live at various levels of activity at any given time. 

<ul> 
{% for proj in site.projects %}
  {% if proj.status != "wootness" %}
    <li>
      <a href="{{proj.url}}.html">{{proj.title}}</a>
    </li>
  {% endif %}
{% endfor %}
</ul>


## Onboarding

If you're joining in any of the projects we're working on, you should take a look at the [group onboarding documentation]({{site.baseurl}}/corounum/onboarding.html). This will walk you through the tools we use in our work; go ahead and create some accounts. Ask questions of Matt or anyone else in the group if you get stuck.

## Contributors

...

## Alumni

... 

### About the Group's Name

*Lynx Rufus* is the North American bobcat. The bobcat is the Bates College mascot.

*Coro unum* is "one horn" in Latin. 

One-horned bobcats are rarer than unicorns, for the record.