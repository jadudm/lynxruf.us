---
layout: default
title: Home
---

<h3>Courses On Offer <em>Right Now</em></h3>

* [DCS 202: Nature of Data, Data of Nature](https://trello.com/b/rjR4hzbj/dcs-202-course-board)


<hr>

<h2>Research Projects</h2>
<ul> 
{% for proj in site.projects %}
  {% if proj.status != "wootness" %}
    <li>
      <a href="{{proj.url}}.html">{{proj.title}}</a>
    </li>
  {% endif %}
{% endfor %}
</ul>

<hr>

<h2>Notes and Reflections <a href="atom.xml"> <img src="{{site.base}}/images/rss.png" style="margin-bottom: 10px;"></a></h2>
<ul>
  {% for post in site.posts limit:32 %}
    <article class="post_card post">
      <header class="post_header">
        <li><a href="{{ post.url | prepend: site.baseurl }}">{{ post.title}}</a> <small style="font-size:70%; color:#AAAAAA;">{{post.categories | last | upcase}}</small><br> 
        <small style="color:#AAAAAA;">{{post.author}} - {{ post.date | date: "%Y%m%d"}}</small>
        </li>
      </header>
    </article>
{% endfor %}
</ul>

<!--
  <hr>
  <div class="row">
    <div class="pagination older-post"><a href="./archive"><data data-icon="ei-arrow-right"></data> Archive  </a></div>
  </div>
</div>
-->