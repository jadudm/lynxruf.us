---
layout: default
title: Courses
---

# 2019

## Winter Term

* [DCS 202: Nature of Data, Data of Nature](https://trello.com/b/rjR4hzbj/dcs-202-course-board) (w/ Shrout)
* Independent Study: Environmental Sensing (1x)
* Independent Study: Language Design and Implementation (4x)
* Independent Study: Interactive Sound Art (w/ Bolaños) (2x) 

# 2018

## Fall Term

* [DCS 103: People, Prose, and Programming](/courses/dcs103f18)

## Short Term

* [DCS s12: Community Engaged Computing](dcs12s18/)

## Winter Term

* [DCS102: Design of Computational Systems](dcs102w18/)
* [DCS202: Nature of Data, Data of Nature](dcs202w18/)

# 2017

## Fall

* [DCS102: Design of Computational Systems](https://jadud.com/teaching/2017f-dcs102/)
