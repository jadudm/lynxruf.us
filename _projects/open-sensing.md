---
title: Affordable Open Hardware for Underwater Citizen Science 
blurb: Cheap, abiotic monitoring of coastal waters
repository: 
trello: 
repos-host: Bitbucket
contributors: mjadud
status: active
---


## Team

* Maddie Hallowell '20
* Liz Johnson '20
* Phil Dostie (Geology)

## Design Constraints

*All of our [sensing projects operate under the same broad design criteria]({{site.baseurl}}/corounum/projects/sensing.html).*

We have a challenging set of design goals for our sensing platforms:

* **Battery life** needs to be on the order of *seasons* not days or weeks. When retrieved, we'd like them to be *rechargeable*.
* **Daily offload** of data is ideal. Acceptable in some cases is that data is available at the end of season.
* **Sensors are precise, accurate, and ideally modular**, so that new deployments do not require the design of a whole new sensor.
* **Low cost** sensors allow more, not less, coverage of bodies of water.
* **Easily assembled** from commercial, off-the-shelf components, potentially enabling *citizen science* around water quality and health.

## Current Status

Our sensor design is a series of *fillings* that make up a *sensor sandwich stack*. Using a common connector, each layer plugs into the other providing a robust connection that allows us to create modular sensors appropriate to any given use case, while also keeping each individual *filling* simple in design (and therefore also simple to debug and test).

Our board design is intended to go into a cylindrical enclosure---like PCB tubing available at most hardware and plumbing supply stores---that we would then fill with marine-grade epoxy, thus ensuring that our sensors are waterproof.

Our [designs are available online at Bitbucket](https://bitbucket.org/corounum/).

<table>
  <tr>
    <td valign="top" width="60%"><h3>Controller</h3>
      The controller layer is complete and tested; it is a processor and a clock.</td>
    <td> <img src="{{site.baseurl}}/images/stack/controller-190308.png" align="right" width="200">
    </td>
  </tr>
  <tr>
    <td valign="top"><h3>Pressure/Temperature Sensor</h3>A barometric pressure and temperature layer is complete and tested; this layer is intended to be potted against the outer layer of our sensor, allowing us to log water depth and temperature reliably.</td>
    <td> <img src="{{site.baseurl}}/images/stack/sensor-190308.png" align="right" width="200">
    </td>
  </tr>
  <tr>
    <td valign="top"><h3>Data Storage</h3>The storage layer is designed and untested. We are currently planning on using two EEPROMs as our storage medium. We have not yet added the circuitry to switch the EEPROMs on and off. (That said, our  chosen EEPROMs have a standby mode that draws in the sub-microamp range.)
    </td>
    <td> <img src="{{site.baseurl}}/images/stack/storage-190308.png" align="right" width="200">
    </td>
  </tr>
  <tr>
    <td valign="top"><h3>Power Conditioning</h3>
      The power layer both provides a step-down from the battery pack to 3.3V, as well as recharge circuitry to allow the sensor to be pulled from the research environment, recharged, and redeployed without breaching the integrity of the sensor itself.
    </td>
    <td> <img src="{{site.baseurl}}/images/stack/power-190308.png" align="right" width="200">
    </td>
  </tr>
</table>

<p>&nbsp;</p>

A generic analog sensor layer is yet to be designed, as is a generic digital sensor layer.

We expect to begin testing of our stack "in the wild" by May of 2019.

## Related Publications

* [Hardware Designs (Bitbucket)](https://bitbucket.org/corounum/)

* [The Siren Song of Open Hardware](https://pult.us/u/siren-song)
  <br>Jadud, Hichilo<sup>&#x2605;</sup>, Mupiwa<sup>&#x2605;</sup>, Ray<sup>&#x2605;</sup>, Mahoney

<sup>&#x2605;</sup> Undergraduate research collaborator

## Past Contributors

* Michal Cwik '20
