---
title: Exploring Novice Programming Behavior
blurb: Understanding how beginners learn to program
repository: 
trello: 
repos-host: Bitbucket
contributors: mjadud
status: active
---


## Team

At Bates:

From Away:

* Stephen Edwards (VT)
* Manuel Pérez-Quiñones (UNCC)


## Research Direction

We are interested in the behaviors that make novice programmers successful as well as developing tools that support students in being successful. 

## Current Status

We are currently funded under the NSF (1625630), [Collaborative Research: Promoting a Growth Mindset Using Automated Feedback](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1625630). We are deep in the data gathering and analysis phase of the work.

## Related Publications

* [Aggregate Compilation Behavior: Findings and Implications from 27,698 Users](https://dl.acm.org/authorize?N45525)
  <br>Jadud, Dorn

* [Predicting at-risk novice Java programmers through the analysis of online protocols](https://dl.acm.org/authorize?N84278)
  <br>Tabanao, Rodrigo, Jadud

* [String formatting considered harmful for novice programmers](https://www.tandfonline.com/doi/abs/10.1080/08993408.2010.507335)
  <br>Hughes<sup>&#x2605;</sup>, Jadud, Rodrigo

* [Flexible, reusable tools for studying novice programmers](https://dl.acm.org/authorize?N84271)
  <br>Jadud, Henriksen

* [Methods and tools for exploring novice compilation behaviour](https://dl.acm.org/authorize?N84274)
  <br>Jadud

<sup>&#x2605;</sup> Undergraduate research collaborator.

## Past Contributors

* [Michael Hughes](https://www.michaelchughes.com/)
  <br>Assistant Prof, Department of Computer Science, Tufts

* Finan Browne '21
