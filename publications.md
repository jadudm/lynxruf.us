---
layout: default
title: Publications
---

<ol>
{% for pub in site.data.publications %}
  <li>{{pub.title}}</li>
{% endfor %}
</ol>