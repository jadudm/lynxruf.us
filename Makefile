test:
	jekyll s -P 8080

save:
	git add -A
	git commit -am "Autosave."
	git push

all:
	@echo "'make upload' to build and upload the site."
	
build:
	jekyll build --baseurl https://lynxruf.us

#		scp -r _site/* lynxrufus@lynxruf.us:/home/lynxrufus/lynxruf.us/	
upload: build
	rsync -avr -e "ssh" \
		--delete-after --delete-excluded \
		--filter='P courses/*' \
		_site/ \
		lynxrufus@lynxruf.us:/home/lynxrufus/lynxruf.us/

