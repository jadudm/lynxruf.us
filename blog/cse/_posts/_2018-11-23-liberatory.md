---
layout: post
title:  A Loving Nature of Data
author: mjadud
---

<style>
.row {
  display: flex;
  flex-wrap: wrap;
  padding: 0 4px;

}

/* Create four equal columns that sits next to each other */
.column {
  flex: 20%;
  max-width: 20%;
  padding: 0 4px;
  margin: auto;
}

.column img {
  margin-top: 8px;
  vertical-align: middle;
  margin-left: auto;
  margin-right: auto;
}
</style>

**Conclusion**: I cannot engage in anti-sexist/anti-racist teaching and learning without engaging lovingly.

Next term, I'm co-teaching *Nature of Data, Data of Nature* (NoD/DoN) with my colleague [Dr. Anelise H. Shrout](http://www.anelisehshrout.com/blog/). The course was envisioned (in broad strokes, over a year ago) to be where "data structures meets environmental justice." Given that the one run of the course met in a difficult time slot (7:30PM to 10PM, once per week), I've been doing what I often do before thinking about how to structure/restructure a course: going back to first principles.

## excuses, or invoking the muse

The ideas I am working through in this essay are not yet fully formed. This reflection is written at a moment in time that is simultaneously part of larger life arcs of teaching and learning over the past 10 years, as well as my lived experiences of the past year and even the two months. These arcs are largely invisible to me and to others, and in this reflection, I am attempting to make some of that learning visible.

The invisibility of this learning reminds me of the work of Bridget Riley&mdash;an artist perhaps best known for her op art and systematic exploration of line and color. Riley's work today is not _necessarily_ the obvious endpoint given her beginnings decades ago.

<div class="row" style="margin-bottom: 20px;"> 
  <div class="column">
    <img  src = "{{site.baseurl}}/images/riley-kiss-400.jpg">
    <br><center><small style="font-size: 40%;">Kiss, 1961</small></center>
  </div>
  <div class="column">
    <img src = "{{site.baseurl}}/images/riley-squares.jpg">
    <br><center><small style="font-size: 40%;">Movement in Squares, 1961</small></center>
  </div>
  <div class="column">
    <img src = "{{site.baseurl}}/images/riley-cataract-3.jpg">
    <br><center><small style="font-size: 40%;">Cataract 3, 1967</small></center>
  </div>
  <div class="column">
    <img src = "{{site.baseurl}}/images/riley-achaean.jpg">
    <br><center><small style="font-size: 40%;">Achaean, 1981</small></center>
  </div>
</div>

There are many ways to criticize the personal reflection that I'm undertaking here. In writing it, I am trying to work through complex ideas, and fundamentally believe that with regards to the complexities of the human condition, and the socially constructed ideas of race and gender. This is an exploration of ideas I intend to share with colleagues and students, and hope that it makes clear 1) my commitment to creating safe spaces for my students to meaningfully engage with and be transformed by conversations about race and gender as it relates to computing and data, 2)  to highlight the complexity of creating space to educate and onboard young men into an intersectional space that may be new and challenging to them, and 3) more broadly, that all of us are on a journey of self-discovery and learning, and that my perspectives are not necessarily unique or sage, but they are mine, and I share them so that they can be yet another space for engagement and mutual learning.

# bell hooks... 

_... we cannot claim to love if we are hurtful and abusive._

In _All About Love_, hooks explores affection, respect, care, trust, open/honest communication as dimensions of **love**, which are in contrast to the romanticized, domineering, controlling, or other popular or lived notions of "love" that are too often the norm for too many. The idea of a love for self and others that hooks describes is the starting point for my thinking about redesigning NoD/DoN, and I believe that this framing is important if I am going to navigate with my students experience of learning about design and programming in socially and personally complex and meaningful ways.

{% comment %}
How you (the reader) read words like _respect_, _care_, and _trust_ as they relate to love&mdash;how you _hear_ them, in your mind&mdash;might be difficult. You might not love yourself. In other words, you might not respect yourself and the work you do every day. You might not trust yourself to speak your mind, or fear to speak freely because of the fear of consequence or how you will be judged by others. You might struggle with open and honest conversation with friends, colleagues, and perhaps even family members or life partners. I say you _might_; these challenges are true for me in many ways, and I believe these things are sometimes true for my students. Doubt, fear, uncertainty... these are too often the stock and trade of a students' experience of education today. 
{% endcomment %}

hook's notion of love matters immensely to me if I am going to ask my students to engage with challenging subjects and take risks. For some of my students, every test, essay, lab report, and presentation can serve as another stone in a pile of judgement. (The typical model in higher education classrooms in the US is that you aspire to a 4.0&mdash;a perfect grade&mdash;and everything else is _less than perfect_. It can be exceedingly demoralizing.) To engage them in an educational context that is fundamentally anti-sexist and anti-racist, and to ask them to engage with material in this frame, suggests that I must create a place of trust, respect, and care that students might be unaccustomed to in the classroom, and more importantly, _might not carry into the room themselves, for themselves_.

While an intersectional lens might suggest that some of my students carry more privilege or power into the classroom because of their embodiment, I want to clearly signal that I am thinking at the level of the individual student and their personal, lived experience as they enter my classroom. I feel hooks balances this well in her writing: the coincidence, contrast, and sometimes conflict of the individual and the systemic, the interplay of power, privilege, and the person. Keeping care and respect at the forefront helps me remember that the people who are in the classroom with me are not a collective, they are not systems of power unto themselves, but instead human beings with fears, doubts, hopes, and joys, not always in equal measure at any given moment in time. 

## ... and men.

I have engaged students in writing and reflection regarding issues of justice and equity in a wide variety of computing contexts. If I reflect on the readings and writings that I've assigned, and the conversations I've had with my students in and out of the classroom, it becomes apparent that my teaching has largely been about trying to help students see the world from a perspective other than their own. I am often trying to cultivate _empathy_, and am perhaps (consciously or unconsciously) trying to help young (cis) men cultivate an awareness of life beyond a lived embodiment akin to their own. Ultimately, I believe the way I situate this learning empowers all of my students to engage in this learning, a claim for which I have collected evidence from course evaluations and group interviews over the years.

When I step back, however, I believe this could be described as an incomplete education.

In his essay [The Boys are Not All Right](https://www.nytimes.com/2018/02/21/opinion/boys-violence-shootings-guns.html), Michael Ian Black explores masculinity today, summarizing the Catch 22 that young men face as they grow into themselves; a "man who feels lost but wishes to preserve his fully masculine self has only two choices: withdrawal or rage." Andrew Reiner [writes about his honors seminar at Towson](https://www.nytimes.com/2016/04/10/education/edlife/teaching-men-to-be-emotionally-honest.html?module=inline) titled Real Men Smile, and describes how roughly 1 in 5 of the young men who take his course match national profiles: "When I asked one of my male students why he didn’t openly fret about grades the way so many women do, he said: 'Nothing’s worse for a guy than looking like a Try Hard.'" Claire Cain Miller captures valuable perspectives in this theme in both her essays [How to Raise a Feminist Son](https://www.nytimes.com/2017/06/02/upshot/how-to-raise-a-feminist-son.html?module=inline) and [Many Ways to Be a Girl, but One Way to Be a Boy](https://www.nytimes.com/2018/09/14/upshot/gender-stereotypes-survey-girls-boys.html), the latter referencing the Plan International report titled [The State of Gender Equality for U.S. Adolescents](https://www.planusa.org/docs/state-of-gender-equality-summary-2018.pdf), which bears further study. From the summary: "While adults say that honesty, morality, ambition, and leadership are the traits that society values most in men<sup><a href="http://www.pewsocialtrends.org/2017/12/05/on-gender-differences-no-consensus-on-nature-vs-nurture/">*</a></sup>, boys are much more likely to say that society most values strength and toughness in boys (35 percent). Only two percent say honesty/morality and eight percent say ambition or leadership."

As I reflect back on these and other pieces I've read over the years, I realize that I have been attempting to help my students think empathetically, but it is almost always extrospective, not introspective. From _Feminist Theory: from margin to center_, in the chapter "Men: Comrades in Struggle," bell hooks writes


{% comment %}
It is [all fine and good to say](https://medium.com/gender-theory/all-men-are-the-enemy-feminist-movements-are-never-just-a-womans-job-b7b0cbfc6002) that men need to engage in the feminist agenda, and that anti-sexist work requires _all_ of us to engage. As I reflect on my own learning, my own engagement, and how my own lived experience resonates with both popular pieces like those referenced above (and scholarly literature in this space as well), I am drawn back to hooks and her notion of _love_, and how critical it is that I acknowledge and honor the underlying human issues that the women and men entering my classroom carry with them if I am going to ask them to engage meaningfully with personal learning that asks them to care and respect themselves and others. 
{% endcomment %}


<blockquote>
All men support and perpetuate sexism and sexist oppres­sion in one form or another. It is crucial that feminist activists not get bogged down in intensifying our awareness of this fact to the extent that we do not stress the more unemphasized point which is that men can lead life affirming, meaningful lives without exploiting and oppressing women. Like women, men have been socialized to passively accept sexist ideology. While they need not blame themselves for accepting sexism, they must assume responsibility for eliminating it. It angers women activists who push separatism as a goal of feminist movement to hear emphasis placed on men being victimized by sexism; they cling to the "all men are the enemy" version of reality. Men are not exploited or oppressed by sexism, but there are ways in which they suffer as a result of it. This suffering should not be ignored. While it in no way diminishes the seriousness of male abuse and oppression of women, or negates male respon­sibility for exploitative actions, the pain men experience can serve as a catalyst calling attention to the need for change.
</blockquote>

hooks laid the groundwork in the early 1980's for the pieces that I reference above that were written in the 2010's. _Men are not exploited or oppressed by sexism, but there are ways in which they suffer as a result of it._ This is a theme that has largely been missing from my own thinking and discussion with students about race, gender, power, and privilege in computing. Or, better put, the readings, reflections, and conversations my students and I engage with are often about how everyone other than men struggle in a sexist/racist/patriarchal society.

Is my blindness to this imbalance in my own practice because I have lived it? Perhaps it is because I am afraid to engage students in questions of maleness or masculinity, in part because I lack the theoretical frame for it, and no real model for how these conversations might be tackled in or out of the classroom. Perhaps this theme is absent because, despite awareness, it is easy to slip into deficit models when it comes to education: (white) men in the US context are perpetuators and benefactors of structural power, and therefore _others are not_. As a result, it is easy to forget that young men, while they might grow into propagators of the imperialist white supremacist capitalist patriarchy, 

I think I am trying to say that I have more reading, reflection, and inner work to do as I think about what hooks has to say. Regardless, I believe I can say that young people&mdash;regardless of how they identify and present&mdash;in my classroom must have space to question and grow (healthily) if they are going to engage with and understand the structural inequities in computing and related disciplines and their relationships to, with, and without those structures of power and privilege. 

## the challenge for data feminism

In no small part, my reason for this reflection is my initial read of [Data Feminism](https://bookbook.pubpub.org/pub/c5vkehnm), a new MIT Press text currently under open review by D'Ignazio and Klein. I have read (most of) this text because my colleague is interested in using it as a course text for NoD/DoN. I struggled to identify the source of my tensions and concerns about the text. As an educator who is working hard to encourage my students in technical fields to see every one of their classmates as unique and valuable human beings worthy of respect and care, I believe the text fails to foster an equitable and caring lens that will help me build the classroom culture that I aspire to.

To pick examples from four chapters:

* **Chapter 2**. "'But!' you might say. 'Data science is premised on things like objectivity and neutrality!...'" It is unclear who the reader of the text is. Is the reader a student new to the subject, or an expert who agrees with the author? For a student who has never engaged with these ideas before, the second person can be very distracting.
* **Chapter 3**. "'Sign in or create an account to continue.' These may be the most unwelcome words on the internet." Hyperbole. Or, a truth for some, but not even consistently for all in a given class. Throughout the text, I personally feel that hyperbole weakens the otherwise excellent work of the authors.
* **Chapter 3**. "Think back to middle school, when you likely learned..." Again, the second person that explicitly assumes things about the reader, who they are, where they're from, what they've learned...
* **Chapter 3**. "No one but a gender non-conforming person would know that..." This demeans and assumes what readers who are not gender non-conforming will not know. If someone is wrestling with their gender identity, and don't know this, it further demeans them as well. It is hurtful, and a wholly unnecessary comment.
* **Chapter 5**. _Big Dick Data_. In using the male genitalia and its size as a way to criticize the work of colleagues, the authors choose to use demeaning language that is [often linked with male perceptions of body image and self worth](https://journals.sagepub.com/doi/abs/10.1177/1359105308095971). If young men in my classes are struggling with notions of self and growing into/learning how to engage in healthy and meaningful relationships with classmates (regardless of gender), what does this choice of language and criticism (attack?) (dis)empower, and to what end? Many men are born with penises: _they can't help it_. Why is this an acceptable form of discourse in a text that is apparently trying to preach equity and intersectionality? 
* **Chapter 8**. "it is no wonder that people who have been socialized into this world order are terrible at creating inclusive visions of how technology might actually be used for collective benefit." While it is [absolutely true that women are capable of perpetuating the patriarchy every bit as much as men](https://www.nytimes.com/2018/11/23/opinion/sunday/sheryl-sandberg-facebook-russia.html), it is easy to read this sentence in the immediate context from which it is taken, as well as the larger context of the text, as damning men for being men. To be clear, I believe the idea the authors are expressing has a great deal of truth to it, but it does not need to be judgemental. I would offer "It can be extremely difficult for people socialized into&mdash;who have lived their entire lives invisibly and continuously benefiting from&mdash;structures of power and oppression to engage with the discomfort of questioning their place of power..." This is an alternative framing that creates a space for growth and dialogue, as opposed to establishing the text as a place of judgement and dismissal.  

I find that there are often cases throughout the text where citations or references are not provided for quotes and information offered up as facts or "truths" with a small "t." These only bother me because the authors lay out the case for citation and biography of data so clearly in the text. But what is particularly difficult for me is that engaging with learners on a journey is literally part of the remit the authors have set for themselves... and to me, it feels as I read the text, that the authors are not engaging all of their learners from a place of care, respect, and trust. While they cite and claim a liberatory frame, with nods to hooks and related scholars, the tone and character of the engagement too often feels more critical than caring, more skeptical than trusting.

For me, this challenges my understanding of hooks, from reading and conversation. 

## the danger of essentializing

The words "I worry" mean "I am still living in a space of dissonance with." It is a statement of tension, and uncertainty, and preparedness for growth and learning.

1. I worry that my own reflections essentialize&mdash;center as a duality&mdash;masculinity, femininity, and the lived experiences of students in my classroom. 
1. I worry that this reflection uses the terms "young men" throughout, and as such, may illustrate an enculturated difficulty with engaging with these ideas intersectionally.
1. I worry that I am expecting a more "rational," and less "emotional" treatment of a difficult topic, or a decentering of the author (the "god view") as I read _Data Feminism_. 

If I grant myself a modicum of self-trust and space for these worries, I might answer:

1. I am reflecting on my own instructional practices, which have largely been invisible to colleagues (but lived experiences for my students), and know that I have left consistently left men out of my framing. I believe that I have provided space for women and LGBQT students to engage (privately, or publicly if they choose) from their own lived perspectives, but again, I would have to look carefully at assignments to be certain they were not overly heteronormative.
1. I admit that am intentionally focusing on the absence of a theoretical framing for the lived experience of young men who, left unengaged, too easily go on to live lives that tacitly and explicitly reinforce the imperialist white supremacist capitalist patriarchy. If I am unwilling to create a space where *their* fears, concerns, and hurts have no validity, then it will be unsurprising if I fail to achieve my goal of creating a caring, trusting space for them to grow as human beings (and, in growing and learning, we hope they then outgrow the hurtful and socialized space in which they have likely lived their lives).
1. I like _Data Feminism_. Ultimately, I am OK with some glossing of citation and detail around numbers that are easily double-checked. I don't even mind the occasional lightness of tone in the text&mdash;for reals! However, I personally find the interjections of the second person demeaning and damaging in places, and it happens often enough in my first read that I found myself not wanting to read further. More importantly, I found myself struggling to imagine engaging my students with the text. What I came to realize as I read and reflected was that I want, more than anything, is for the language of the text to "walk the talk" of a caring and liberatory frame&mdash;because, I believe, the text is close, and because I believe the authors can. 

## conclusion

I cannot engage in anti-sexist/anti-racist teaching and learning without engaging lovingly. 

First, this means that I need to care, respect, and trust my own expressions of thought and learning, even when I know that I will be judged wanting for those expressions... and, perhaps, judged rightly. This is part of the learning process. _I read, I reflect, I discuss, I learn, and I repeat_.

Second, this means I need to care, respect, and trust my students. In doing so, I need to trust and care for _all of my students_. It is easy to create environments where some of my students feel more welcome than others; we know from literature regarding bias. Conversely, it also means that it is easy for me to create environments where some of my students feel more _judged_ than others. In either case, the question of whether I am creating an environment that fosters reciprocity and trust for _all_ of my students (with a mind to [Chickering and Gamson's Seven Principles](http://www.lonestar.edu/multimedia/sevenprinciples.pdf)) is at the front of my mind as I reflect on what I do and why I do it in service to my students' growth and learning.

Finally, it means that I need to find ways to help foster dialogue within my new  community at Bates that centers these values. I have only been at Bates for a little over a year, and that has been a complex year. I have often found myself in a space of feeling guilty or aware of the fraught space of privilege I was hired into (with tenure and an endowed chair). DCS is a new program, with multiple faculty lines, established more by top-down fiat than ground-up community desire (at least, that is the predominant narrative I have heard, repeatedly). Added to this an awareness of the many layers of attendant structural privilege this entails, and it is hard to know how best to engage productively with what amounts to a systemic change agenda in a community that may, or may not, be ready for or even welcome change.

<!-- If I had one ask, it would be for _time_. Time to learn, time to build relationships, and time to understand the structures of power that I want to see us tear down and rebuild, so as to create spaces for our students that foster caring and respectful interactions.
-->

