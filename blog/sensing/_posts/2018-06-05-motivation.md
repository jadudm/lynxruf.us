---
layout: post
title:  "Motivation for a New Embedded Formfactor"
author: mjadud
---

I want to revisit a fundamental design premise explicitly for the purpose of soliciting feedback. If you've got some background in this space, please drop me a note (mjadud at bates dot edu or @jadudm on Twitter). My students and I would welcome your input

------

I want a low-cost environmental sensing solution. My design criteria/constraints:

* **BASE COST**. It should be possible for me to put a bare-bones sensor together (eg. a controller and enclosure) for less than $10.
* **ENCLOSURE**. The enclosure should be submersible, and able to be built easily with COTS components that can be sourced at a typical US hardware store.
* **MODULAR**. It should be possible for me to build a sensor by adding what I need, and nothing more. For example, if I need WiFi, I should be able to add it; it should not be present "by default" (thus adding cost, complexity). This goes for all aspects of the design: sensing, power, timekeeping, storage, and so on.
* **ABSTRACTED**. Modular layers should be abstracted in hardware and software for ease of use. For example, every hardware component in the sensor stack should implement a common "goToSleep()" command; it should not be different on a per-layer basis, nor should it be something that the programmer needs to look up for different kinds of components. Common interfaces should be common, even if they turn out to be a "no-op" for some classes of hardware.
* **BATTERY-FIRST**. Everything should be designed with the thought that we will be powering our board with a budget of 1000-2000mAh of power, and that we want to last at least 6mo to 1yr on that budget for most applications. At the least, we want to last a summer season.
* **OPEN**. The hardware and software should be free and open.

The current thinking is that we will design against our enclosure choice, which is 2" PVC. It is cheap, commonly available everywhere (it is in most every home and building in the USA), and it is easy to work with (glue, drill, cut, etc.). Smaller-diameter tubing makes design hard, and larger gets expensive. An enclosure like the mockup below costs around $4.

<a href='https://photos.google.com/share/AF1QipOye6UXBSKhEt8hYvMuyWIQmWBxfl5CIJWFtMNsoIqHpU8kxBqXxvg1_eQgd4Wa9g?key=X1FJTzM3U1lPMHYwVS0tS2g5bnFQQU5VSGd0RE5n'><img src='https://lh3.googleusercontent.com/Kg40G6fgSONbiGiVKGjJ59dU_0imJdscVtNcElnRA71EI5gBPqy87p21UHcrWAMWXbYl38Xhho3Gy4Wi-Qhs0HxOTkph3GZnyLxvMBrXSLeCMLrRxDbLHB_nAITnCXrRgXpDfUVakfQ=w2400' /></a>

This design choice restricts our selection of electronics hardware.

* **Adafruit Feathers**. The feather is a 2" board, which will not fit in a 2" diameter cylinder. While it could be rotated 90&deg;, this limits the height of the stack. I also worry about power consumption/deep-sleep on Feather boards in the general case. 
* **Sparkfun Qwiic**. Sparkfun's design only specifies the cable parameters, and nothing about the physical design of components. Mounting the free-form modules in an enclosure becomes difficult. I<sup>2</sup>C as a standard is promising, but boards (generally speaking) lack the modularity and power management features that we require.
* **Seeed Grove**. Nothing about Grove is designed with these parameters in mind; they are all modules of different sizes, and the interconnects vary. We do not win on power management and modularity.
* **Pi Hats**. There's nothing in the Raspberry Pi space that is appropriate for this kind of work, given the power consumption on the Pi.

The physical board is currently shaping up to be a 1.8" wide octagon (allowing space for cabling around the board in a 2" diameter cylinder), and two headers spaced 1.4" apart. Header 1 gives us VREG, GND, "wake", SCL, SDA, and VBATT. Header 2 gives us VREG, GND, SS, MOSI, MISO, and SCK. We get both unregulated and regulated power on each layer (VREG and VBATT), I<sup>2</sup>C, and SPI. The "wake" pin will be pulled low (or high, TBA), and toggling the line will signal to the entire stack that it should begin wakeup. (This behavior is tentative at the moment.)

<a href='https://photos.google.com/share/AF1QipPb1Bu36fEzdRkeKORYlA-lGl0bSLqjYBYpuaIAK9Zevi50UJ4oXMJwoG8rLoVBiA?key=Q3BpM3U0QUZTaEN3UV9KV2swVFBhckhWUHVSRld3'><img src='https://lh3.googleusercontent.com/ZkmtvR1K5cgVFt47M90_gY_MZeEmldEtsJvRmQbL9qLupsUCzCHDP4B09bdIyt4eC1Us3ZQD_A82AMN76zWiof3yS50aSsuAX3488o0S2n4ZnGsLOa-5UBq7inxXqsqpVoH4BmlbKmc=w2400' /></a>

(As I look at the board, I worry about plugging this in backwards. I might want to offset one of the headers by a single 0.1" step, so that it is impossible to plug them into each-other "wrong," but they remain breadboard compatible.)

My concern is that I've failed to ask myself enough questions, and that I'm reinventing wheels that don't need to be reinvented. That said, I don't believe there's a platform that I can pull "off the shelf" and use robustly/reliably, semester-after-semester and year-after-year, with my students on a wide variety of (as-of-yet unspecified) environmental sensing projects. 

## Next Steps

I have three students working with me this summer on this project; we'll be blogging more about it as we proceed, and we'll be building prototypes with our [Voltera V-One](https://www.voltera.io/). Hopefully, we're on a sane path...







