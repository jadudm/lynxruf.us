---
layout: page
title: About
permalink: /about/
---

This website provides a home for the teaching and research of [Matt Jadud](https://jadud.com/) at [Bates College](https://bates.edu/). Matt is the Colony Family Chair in Digital and Computational Studies.